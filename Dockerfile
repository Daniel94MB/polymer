# Imagen base
FROM node:latest

# Carpeta raiz del contenedor
WORKDIR /app

# Copiado de archivos
ADD build/default /app/build/default
ADD server.js /app
ADD package.json /app

# Dependencias 
RUN npm install 

# Puerto que exponemos
EXPOSE 3000

# Comando para ejecutar al aplicacion
CMD ["npm","start"]

